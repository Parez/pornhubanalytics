import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {HomeSlideComponent} from './slides/home-slide/home-slide.component';
import {InterestingFactsSlideComponent} from './slides/interesting-facts-slide/interesting-facts-slide.component';
import {CategoriesSlideComponent} from './slides/categories-slide/categories-slide.component';
import {EndingSlideComponent} from './slides/ending-slide/ending-slide.component';
import {InterestingFacts2SlideComponent} from './slides/interesting-facts2-slide/interesting-facts2-slide.component';


const routes: Routes = [

  { path: 'login', component: HomeSlideComponent },
  { path: 'facts', component: InterestingFactsSlideComponent},
  { path: 'facts2', component: InterestingFacts2SlideComponent},
  { path: 'categories', component: CategoriesSlideComponent},
  { path: 'ending', component: EndingSlideComponent},
  { path: '**', redirectTo: "login" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
