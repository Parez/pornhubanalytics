import {INameValuePair} from './INameValuePair';
/**
 * Created by baunov on 07/08/2017.
 */
export interface ICategoryEmotions
{
    category:string;
    emotions:INameValuePair[];
}