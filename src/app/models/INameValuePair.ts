/**
 * Created by baunov on 07/08/2017.
 */

export interface INameValuePair
{
    name:string;
    value:number;
}