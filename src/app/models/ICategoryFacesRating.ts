/**
 * Created by baunov on 08/08/2017.
 */

export interface ICategoryFacesRating
{
    category:string;
    facesPercent:number;
    rating:number;
}