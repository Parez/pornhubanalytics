/**
 * Created by baunov on 07/08/2017.
 */
 export class ITimedEmotions
{
    length_percent: number;
    anger: number;
    contempt:number;
    disgust:number;
    fear:number;
    happiness:number;
    neutral:number;
    sadness:number;
    surprise:number;
}