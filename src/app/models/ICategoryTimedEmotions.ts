import {ITimedEmotions} from './ITimedEmotions';
/**
 * Created by baunov on 07/08/2017.
 */

export interface ICategoryTimedEmotions
{
    category: string;
    times: ITimedEmotions;
}