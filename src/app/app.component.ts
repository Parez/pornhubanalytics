import {Component, OnInit} from '@angular/core';
import {DataService} from './services/data.service';
import {Observable} from 'rxjs';
import {ICategoryEmotions} from './models/ICategoryEmotions';
import {INameValuePair} from './models/INameValuePair';
import {PresentationService} from './services/presentation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{



  constructor(private presService:PresentationService)
  {

  }

  ngOnInit(): void {
    this.presService.initSlides(["/login", "/facts", "/facts2", "/categories", "/ending"]);
  }


  private filterNeutral(cat:ICategoryEmotions):ICategoryEmotions
  {
    let neutral = cat.emotions.filter(emo => emo.name === "neutral")[0].value;

    console.log(neutral);

    let newEmo:INameValuePair[] = cat.emotions.map(emo => {
      return Object.assign({}, emo, {value: emo.value/(1-neutral)});
    }).filter(emo => emo.name !== "neutral");

    console.log(newEmo);
    return {category: cat.category,emotions: newEmo};
  }

  //filterEmotion("")
}
