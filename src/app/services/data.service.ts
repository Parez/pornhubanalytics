import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {ICategoryEmotions} from '../models/ICategoryEmotions';
import {Observable} from 'rxjs';
import {ICategoryTimedEmotions} from '../models/ICategoryTimedEmotions';
import {ICategoryFacesRating} from '../models/ICategoryFacesRating';


@Injectable()
export class DataService {


  private emoTotals$:Observable<ICategoryEmotions[]>;
  private emoTotals2$:Observable<ICategoryEmotions[]>;
  private timedEmo$:Observable<ICategoryTimedEmotions[]>;
  private facesRating$:Observable<ICategoryFacesRating[]>;

  public selectedCategory:string = "";

  constructor(private http:Http) { }

  public getEmoTotals():Observable<ICategoryEmotions[]>
  {
    if(this.emoTotals$) return this.emoTotals$;

    this.emoTotals$ = this.http.get("./assets/data/emo_totals.json").map( (res:Response) => {
      return res.json().map(this.changeFormat).sort(this.categoryCompare)
    }).publishReplay(1).refCount();

    return this.emoTotals$;
  }

  public getEmoTotals2():Observable<ICategoryEmotions[]>
  {
    if(this.emoTotals2$) return this.emoTotals2$;

    this.emoTotals2$ = this.http.get("./assets/data/emo_totals_2.json").map( (res:Response) => {
      return res.json().map(this.changeFormat).sort(this.categoryCompare)
    }).publishReplay(1).refCount();

    return this.emoTotals2$;
  }

  public getTimedEmotions():Observable<ICategoryTimedEmotions[]>
  {
    if(this.timedEmo$) return this.timedEmo$;

    this.timedEmo$ = this.http.get("./assets/data/timed_emotions.json").map( (res:Response) => {
      return res.json().map(this.changeTimedFormat).sort(this.categoryCompare)
    }).publishReplay(1).refCount();

    return this.timedEmo$;
  }

  public getFacesRating():Observable<ICategoryFacesRating[]>
  {
    if(this.facesRating$) return this.facesRating$;

    this.facesRating$ = this.http.get("./assets/data/faces_rating_cat.json").map( (res:Response) => {
      return res.json().map(this.changeFacesRatingFormat).sort(this.categoryCompare)
    }).publishReplay(1).refCount();

    return this.facesRating$;
  }

  private categoryCompare(a, b)
  {
    if (a.category > b.category) {
      return 1;
    }
    if (a.category < b.category) {
      return -1;
    }
    return 0;
  }

  private changeFacesRatingFormat(elem:any):ICategoryFacesRating
  {
    return {
      category: elem.category,
      facesPercent: elem.percent,
      rating: elem.rating
    }
  }

  private changeFormat(elem:any):ICategoryEmotions
  {
    return {
      category: elem.category,
      emotions: [
        {name: 'злость', value: elem.anger},
        {name: 'презрение', value: elem.contempt},
        {name: 'отвращение', value: elem.disgust},
        {name: 'страх', value: elem.fear},
        {name: 'счастье', value: elem.happiness},
        {name: 'нейтрал', value: elem.neutral},
        {name: 'грусть', value: elem.sadness},
        {name: 'удивление', value: elem.surprise}
      ]
    };
  }

  private changeTimedFormat(elem:any):ICategoryTimedEmotions
  {
    return {
      category: elem.category,
      times: {
        length_percent: elem.length_percent,
        'anger': elem['avg(anger)'],
        'contempt': elem['avg(contempt)'],
        'disgust': elem['avg(disgust)'],
        'fear': elem['avg(fear)'],
        'happiness': elem['avg(happiness)'],
        'neutral': elem['avg(neutral)'],
        'sadness': elem['avg(sadness)'],
        'surprise': elem['avg(surprise)']
      }
    };
  }

}
