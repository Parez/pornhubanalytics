import { Injectable } from '@angular/core';
import {Router, ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd} from "@angular/router";

@Injectable()
export class PresentationService {

  private curIndex:number = 0;
  private routesAr:string[] = [];
  constructor(private router:Router) {
    router.events.subscribe((val) => {
      if(val instanceof NavigationEnd)
      {
        console.log(val.url);
        this.curIndex = this.routesAr.indexOf(val.url);
        if(this.curIndex < 0) this.curIndex = 0;
      }

    });
  }

  public initSlides(ar:string[])
  {
    this.routesAr = ar.slice();
    console.log(this.router.url);
    this.curIndex = this.routesAr.indexOf(this.router.url);
    if(this.curIndex < 0) this.curIndex = 0;
    console.log(this.curIndex);
  }

  public hasNext():boolean
  {
    return (this.curIndex < this.routesAr.length-1);
  }

  public hasPrev():boolean
  {
    return (this.curIndex > 0);
  }

  public next()
  {
    if(this.hasNext()) {
      this.curIndex++;
      this.router.navigateByUrl(this.routesAr[this.curIndex]);
    }
  }

  public prev()
  {
    if(this.hasPrev()) {
      this.curIndex--;
      this.router.navigateByUrl(this.routesAr[this.curIndex]);
    }
  }

}
