import {Component, OnInit} from '@angular/core';
import {ICategoryEmotions} from '../../models/ICategoryEmotions';
import {Observable} from 'rxjs';
import {DataService} from '../../services/data.service';
import {ICategoryTimedEmotions} from '../../models/ICategoryTimedEmotions';
import {INameValuePair} from '../../models/INameValuePair';
import {ICategoryFacesRating} from '../../models/ICategoryFacesRating';

@Component({
  selector: 'app-categories-slide',
  templateUrl: './categories-slide.component.html',
  styleUrls: ['./categories-slide.component.css']
})
export class CategoriesSlideComponent implements OnInit {

  categoryData$:Observable<INameValuePair[]> = null;
  categoryData2$:Observable<INameValuePair[]> = null;
  timedData$:Observable<ICategoryTimedEmotions[]> = null;

  facesRating$:Observable<ICategoryFacesRating> = null;

  categories$:Observable<string[]> = null;



  selectedCategory:string;

  constructor(private dataService:DataService) {


  }

  ngOnInit() {
      console.log("Init");
      this.categories$ = this.dataService.getEmoTotals()
          .map( (cats:ICategoryEmotions[]) => {
              return cats.map(cat => cat.category);
          });
  }


  onCatSelected(val:string)
  {
    this.categoryData$ = this.dataService.getEmoTotals()
        .map( (cats:ICategoryEmotions[]) => {
          return cats.filter(cat => cat.category === val)[0].emotions
        });
    //.map(this.filterNeutral);

    this.categoryData2$ = this.dataService.getEmoTotals2()
        .map( (cats:ICategoryEmotions[]) => {
          return cats.filter(cat => cat.category === val)[0].emotions
        });

    this.timedData$ = this.dataService.getTimedEmotions()
        .map( (cats:ICategoryTimedEmotions[]) => {
            return cats.filter(cat => cat.category === val)
        });

    this.facesRating$ = this.dataService.getFacesRating()
        .map( (cats:ICategoryFacesRating[]) => {
            return cats.filter(cat => cat.category === val)[0]
        });
    //.map(this.filterNeutral);
    this.dataService.selectedCategory = val;
    this.selectedCategory = val.charAt(0).toUpperCase() + val.slice(1);
  }

}
