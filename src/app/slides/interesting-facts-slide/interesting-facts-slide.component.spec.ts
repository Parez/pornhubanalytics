import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestingFactsSlideComponent } from './interesting-facts-slide.component';

describe('InterestingFactsSlideComponent', () => {
  let component: InterestingFactsSlideComponent;
  let fixture: ComponentFixture<InterestingFactsSlideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestingFactsSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestingFactsSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
