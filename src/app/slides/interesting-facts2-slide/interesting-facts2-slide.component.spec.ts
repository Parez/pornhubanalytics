import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestingFacts2SlideComponent } from './interesting-facts2-slide.component';

describe('InterestingFacts2SlideComponent', () => {
  let component: InterestingFacts2SlideComponent;
  let fixture: ComponentFixture<InterestingFacts2SlideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestingFacts2SlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestingFacts2SlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
