import { Component, OnInit } from '@angular/core';
import {INameValuePair} from '../../models/INameValuePair';

@Component({
  selector: 'app-interesting-facts2-slide',
  templateUrl: './interesting-facts2-slide.component.html',
  styleUrls: ['./interesting-facts2-slide.component.css']
})
export class InterestingFacts2SlideComponent implements OnInit {

  percentage:INameValuePair[];
  constructor() { }

  ngOnInit() {
    let emos = [
      {name:"neutral", value: 15019},
      {name:"happiness", value: 7401},
      {name:"surprise", value: 1225},
      {name:"sadness", value: 727},
      {name:"anger", value: 243},
      {name:"fear", value: 56},
      {name:"disgust", value: 21},
      {name:"contempt", value: 17}
    ];

    let total = emos.map(x => x.value).reduce( (a,b) => a+b);

    this.percentage = emos.map(x => {
      return {name:x.name, value: x.value/total};
    });
  }

}
