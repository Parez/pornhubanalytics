import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndingSlideComponent } from './ending-slide.component';

describe('EndingSlideComponent', () => {
  let component: EndingSlideComponent;
  let fixture: ComponentFixture<EndingSlideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndingSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndingSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
