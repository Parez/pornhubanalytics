import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {

  private _selectedLabel:string = "60fps";
  @Input() set selectedLabel(val:string)
  {
    this.selectCat(val);
  }

  @Input() categories:string[];
  @Output() selected:EventEmitter<string> = new EventEmitter();

  selectedCat:string = "";

  constructor() { }

  ngOnInit() {
  }

  selectCat(cat:string)
  {
    this.selectedCat = cat;
    this._selectedLabel = cat;
    this.selected.emit(cat);
  }

}
