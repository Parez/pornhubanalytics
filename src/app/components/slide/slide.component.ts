import {Component, OnInit, Input, HostListener} from '@angular/core';
import {PresentationService} from '../../services/presentation.service';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {

  @Input() showControls:boolean = true;

  constructor(private presService:PresentationService) {

  }

  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    if(event.keyCode == 37) //left
    {
      this.presService.prev();
    }
    else if(event.keyCode == 39) //right
    {
      this.presService.next();
    }
  }

  ngOnInit() {

  }

  onNext()
  {
    this.presService.next();
  }

  onPrev()
  {
    this.presService.prev();
  }

}
