import {Component, OnInit, ElementRef, ViewChild, Input} from '@angular/core';
import {ITimedEmotions} from '../../models/ITimedEmotions';
import {ICategoryTimedEmotions} from '../../models/ICategoryTimedEmotions';
import {D3Service} from 'd3-ng2-service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  private _data:ICategoryTimedEmotions[];
  @Input() set data(val:ICategoryTimedEmotions[]) {
    this.drawChart(val);
  };

  @Input() width:number;
  @Input() height:number;

  @ViewChild('chart') private chartContainer:ElementRef;


  private container;
  private svg:any;
  private d3;
  private chart;
  private angerLine;
  private happyLine;
  private sadLine;

  private x;
  private y;

  private g;
  private margin = 40;
  private path;
  private xAxisLength;
  private yAxisLength;
  public scaleX;
  public scaleY;



  constructor(d3Service:D3Service) {
    this.d3 = d3Service.getD3();
  }

  ngOnInit() {
    this.initSvg();
    this.drawChart(this._data);
  }

  private initSvg() {
    //div that will contain svg
    this.container = this.chartContainer.nativeElement;
    this.createSvg();
  }

  private createSvg() {
    this.xAxisLength = this.width - 2 * this.margin;
    this.yAxisLength = this.height - 2 * this.margin;

    this.svg = this.d3.select(this.container)
        .append('svg')
        .attr('width', this.width + this.margin * 2)
        .attr('height', this.height + this.margin * 2);

    this.g = this.svg.append("g").attr("transform", "translate(" + this.width / 2 + "," + this.height / 2 + ")");
  }

  //redraw when data arrives
  drawChart(data:ICategoryTimedEmotions[]) {
    if (!data) return;
    if (!this.svg) return;

    data = data.sort(function(a,b) {
      if(a.times.length_percent > b.times.length_percent) {
        return 1;
      } else if(a.times.length_percent < b.times.length_percent) {
        return -1;
      } else {//
        return 0;
      }
    });

    if(this.svg) {
      this.svg.remove();
      this.createSvg();
    }

    if (this.path) {
      this.path.exit().remove();
    }

    this.scaleX = this.d3.scaleLinear().domain([0, 100])
        .range([0, this.xAxisLength]);
    this.scaleY = this.d3.scaleLinear().domain([1,0])
        .range([0, this.yAxisLength]);

    var context = this;

    this.angerLine = this.d3.line()
        .curve(this.d3.curveBasis)
        .x(function (d) {
          return context.scaleX(d.times.length_percent);
        })
        .y(function (d) {
          return context.scaleY(d.times.anger);
        });

    this.happyLine = this.d3.line()
        .curve(this.d3.curveBasis)
        .x(function (d) {
          return context.scaleX(d.times.length_percent);
        })
        .y(function (d) {
          return context.scaleY(d.times.happiness);
        });

    this.sadLine = this.d3.line()
        .curve(this.d3.curveBasis)
        .x(function (d) {
          return context.scaleX(d.times.length_percent);
        })
        .y(function (d) {
          return context.scaleY(d.times.sadness);
        });

    this.svg.append("g")
        .call(this.d3.axisLeft(this.scaleY))
        .attr("transform", "translate(" + this.margin + "," + this.margin + ")")
        .attr("stroke", "orange")
        .attr("fill", "orange");

    this.svg.append("g")
        .call(this.d3.axisBottom(this.scaleX))
        .attr("transform", "translate(" + this.margin + "," + (this.height - this.margin) + ")")
        .attr("stroke", "orange")
        .attr("fill", "orange");

    this.path = this.svg.append("path")
        .data([data])
        .attr("class", "line")
        .attr("transform", "translate(" + this.margin + "," + this.margin + ")")
        .style("stroke", "green")
        .attr("d", this.sadLine);

    this.path = this.svg.append("path")
        .data([data])
        .attr("class", "line")
        .attr("transform", "translate(" + this.margin + "," + this.margin + ")")
        .style("stroke", "red")
        .attr("d", this.angerLine);

    this.path = this.svg.append("path")
        .data([data])
        .attr("class", "line")
        .attr("transform", "translate(" + this.margin + "," + this.margin + ")")
        .style("stroke", "blue")
        .attr("d", this.happyLine);
  }

}
