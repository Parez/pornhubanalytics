import {Component, OnInit, ElementRef, ViewChild, Input, OnChanges, SimpleChanges} from '@angular/core';
import { D3Service } from 'd3-ng2-service';
import {INameValuePair} from '../../models/INameValuePair';

@Component({
  selector: 'app-pie-chart',
  templateUrl: 'pie-chart.component.html',
  styleUrls: ['pie-chart.component.css']
})
export class PieChartComponent implements OnInit, OnChanges {

  private _data:INameValuePair[];
  @Input() set data(val:INameValuePair[])
  {
    if(!val) return;
    this._data = val.sort( (a,b) => {
      if(a.value > b.value) return 1;
      else return -1;
    });
  };

  @Input() minShowVal:number = 0.05;
  @Input() radius:number = 150;
  @ViewChild('chart') private chartContainer: ElementRef;

  private margin = 40;
  private width: number;
  private height: number;

  private arc: any;
  private labelArc: any;
  private pie: any;

  private svg: any;
  private d3;

  private container;

  private chart;

  constructor(d3Service: D3Service) {
    this.d3 = d3Service.getD3();
    this.width = this.radius * 2 + this.margin * 2;
    this.height = this.radius * 2 + this.margin * 2;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.container = this.chartContainer.nativeElement;
    this.d3.select(this.container).select("svg").remove();
    console.log("Pie Chart",this._data);
    this.initSvg();
    this.drawPie(this._data);
  }

  ngOnInit() {
    console.log("Init Pie");

    //this.initSvg();
    //this.drawPie(this._data);
  }

  private initSvg() {
    this.arc = this.d3.arc()
        .outerRadius(this.radius - 30)
        .innerRadius(this.radius/10);
    this.labelArc = this.d3.arc()
        .outerRadius(this.radius - 40)
        .innerRadius(this.radius - 50);

    this.pie = this.d3.pie()
        .sort(null)
        .value((d: any) => d.value);

    this.svg = this.d3.select(this.container)
        .append('svg')
        .attr('width', this.radius*2+this.margin*2)
        .attr('height', this.radius*2+this.margin*2)
        .append("g")
        .attr("transform", "translate(" + this.width / 2 + "," + this.height / 2 + ")");
  }

  private drawPie(data) {
    if(!data) return;
    if(!this.svg) return;

    this.chart = this.svg.selectAll(".arc")
        .data(this.pie(data))
        .enter().append("g")
        .attr("class", "arc");

    this.chart.append("path").attr("d", this.arc)
        .style("fill", "#ff9900" )
        .attr("opacity", (d) => (1-0.3)*(d.index)/data.length+0.3);

    let labels = this.svg.selectAll(".labels")
        .data(this.pie(data))
        .enter().append("g")
        .attr("class", "labels");

    labels.append("text").attr("transform", (d: any) => "translate(" + this.labelArc.centroid(d) + ")")
        .attr("dy", ".35em")
        .attr("text-anchor", "middle")
        .attr("fill", "white")
        .text((d: any) => d.data.value > this.minShowVal ? d.data.name: "");

    labels.append("text").attr("transform", (d: any) => "translate(" + this.arc.centroid(d) + ")")
        .attr("dy", ".35em")
        .attr("fill", "white")
        .text((d: any) => d.data.value > this.minShowVal ? Math.round(d.data.value * 100): "" );
  }
}
