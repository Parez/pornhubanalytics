import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { D3Service } from 'd3-ng2-service';
import { SlideComponent } from './components/slide/slide.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import {DataService} from './services/data.service';
import { CategoriesListComponent } from './components/categories-list/categories-list.component';
import { CategoriesSlideComponent } from './slides/categories-slide/categories-slide.component';
import { HomeSlideComponent } from './slides/home-slide/home-slide.component';
import { InterestingFactsSlideComponent } from './slides/interesting-facts-slide/interesting-facts-slide.component';
import {AppRoutingModule} from './app-routing.module';
import {PresentationService} from './services/presentation.service';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { EndingSlideComponent } from './slides/ending-slide/ending-slide.component';
import {InterestingFacts2SlideComponent} from './slides/interesting-facts2-slide/interesting-facts2-slide.component';

@NgModule({
  declarations: [
    AppComponent,
    SlideComponent,
    PieChartComponent,
    CategoriesListComponent,
    CategoriesSlideComponent,
    HomeSlideComponent,
    InterestingFactsSlideComponent,
    LineChartComponent,
    EndingSlideComponent,
    InterestingFacts2SlideComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [D3Service, DataService, PresentationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
