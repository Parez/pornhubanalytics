import { PornhubEmotionsPage } from './app.po';

describe('pornhub-emotions App', () => {
  let page: PornhubEmotionsPage;

  beforeEach(() => {
    page = new PornhubEmotionsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
